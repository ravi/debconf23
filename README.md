The debconf23 repository is a way to ensure files related to DebConf are
preserved in one place, and to share them with other attendees.

This includes photos (including raw images), videos, talk slides,
documents, GPS tracks, and so on.

Please limit uploads to content which is legal, respectful of others,
and appropriate to share with the world.

We do not have infinite amounts of disk space, so please use common
sense before uploading huge files.

## Uploading slides

If you are uploading slides or some other kind of visual support, please make
sure to upload them in the ['slides'][slides] directory and to name them
properly.

Your file (or your directory) should be named after your talk's URL slug on
the DebConf23 website.

For example, the web page for the [Software Heritage Talk][heritage] is:
`https://debconf23.debconf.org/talks/44-software-heritage-building-a-community-to-safeguard-the-software-commons/`. As such, the
slides for this talk were uploaded in the `slides/44-software-heritage-building-a-community-to-safeguard-the-software-commons/`
directory.

[slides]: https://salsa.debian.org/debconf-team/public/share/debconf22/-/tree/main/slides
[heritage]: https://debconf23.debconf.org/talks/44-software-heritage-building-a-community-to-safeguard-the-software-commons/

## Web

All files in the repository can be accessed via the web:
<https://salsa.debian.org/debconf-team/public/share/debconf23>

## Setup

All Debian developers (DDs) can directly push to this repository.
Others can either open a merge request or ask any DD for commit rights.

The repository uses git LFS (git large file system).
This makes handling of big files more efficient, but requires some
additional attention when adding files to the repository.

1. You need the package `git-lfs`.
1. On Debian systems `# apt install git-lfs` (Buster and later).
1. Run `git lfs install` to set up the "lfs" filter in your system-wide
   git config.

Using `git clone` by itself will download all the large git LFS files
inside the repo, which will be many gigabytes.

Instead, you can do a shallow clone:

1. `$ GIT_LFS_SKIP_SMUDGE=1 git clone --config filter.lfs.smudge=true  git@salsa.debian.org:debconf-team/public/share/debconf23.git`
1. `$ cd debconf23`
1. Using smudge causes files to only be stored as references.
   To actually fetch a file, run `git lfs fetch -I <filename>;git lfs checkout <filename>`.
1. Use the repo as though it were a normal git repository.
   To add files, see below.

## Adding files

Git directs files to LFS, rather than the git repository, when the
filename matches a pattern in the `.gitattributes` file.
This repo is set up to store a number of common large files in LFS.

You can see the files already stored in LFS, by running
`git lfs ls-files`.

Before committing, run `git lfs status` to check that your large files
will be committed to LFS:

```
$ git add DSC_0001.JPG
$ git add VIDEO_0001.MP4
$ git lfs status
On branch main

Git LFS objects to be committed:

	DSC_0001.JPG (LFS: 0eb9017)
	VIDEO_0001.MP4 (Git: a9fe88d)

Git LFS objects not staged for commit:
```

In the above example, `DSC_0001.JPG` will be stored in LFS, but
`VIDEO_0001.MP4` will not.
To fix that, because it is reasonable to expect all MP4 files to be
stored in LFS:

```
$ git lfs track '*.MP4'
Tracking "*.MP4"
$ git add VIDEO_0001.MP4  # Re-adding it, this time to LFS
$ git add .gitattributes
$ git lfs status
On branch main

Git LFS objects to be committed:

	.gitattributes (Git: b8183ad -> Git: e280251)
	DSC_0001.JPG (LFS: 0eb9017)
	VIDEO_0001.MP4 (LFS: a9fe88d)

Git LFS objects not staged for commit:

```

Then, commit and push as usual:

    $ git add slides/testfile.svg
    $ git commit -m"commit message"
    $ git push origin main

## License

License for all files uploaded: CC-BY-SA 4.0 unless explicitly stated otherwise.
